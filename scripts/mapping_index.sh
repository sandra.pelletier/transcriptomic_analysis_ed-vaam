#!/bin/bash

###########################################
# Indexation du transcriptome de référence
###########################################

# Dossier de travail
WKD=~/transcriptomic_analysis_ed-vaam/results/mapping

# Transcriptome de référence
REF=~/transcriptomic_analysis_ed-vaam/data/Araport11_cdna_20220914_representative_gene_model
# Nom de l'index
NAME='index_Araport_20220914'

echo 'dossier de travail:  ' $WKD
echo 'reference:           ' $REF
echo 'index:               ' $NAME

#############################
cd $WKD
salmon index -t $REF -i $NAME
