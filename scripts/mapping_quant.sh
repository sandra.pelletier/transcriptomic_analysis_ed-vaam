#!/bin/bash

###########################################
# Indexation du transcriptome de référence
###########################################

# Dossier de travail
WKD=~/transcriptomic_analysis_ed-vaam/results/mapping
DATA=~/transcriptomic_analysis_ed-vaam/data/

# Transcriptome de référence
SAMPLES=(
  "R1-PW-ZME-Ctrl"
  "R1-PW-ZME-Sc"
  "R1-WS-ZME-Ctrl"
  "R2-PW-ZME-Ctrl"
  "R2-PW-ZME-Sc"
  "R2-WS-ZME-Ctrl"
  "R3-PW-ZME-Ctrl"
  "R3-PW-ZME-Sc"
  "R3-WS-ZME-Ctrl"
)

# Nom de l'index
INDEX='index_Araport_20220914'

echo 'dossier de travail:  ' $WKD
echo 'dossier data      :  ' $DATA
echo 'index:               ' $INDEX
echo 

#############################

for FASTQ in ${SAMPLES[@]}; do
  ls $DATA$FASTQ"_R1.fastq.gz"
  ls $DATA$FASTQ"_R2.fastq.gz"

  salmon quant \
    -l A \
    --validateMappings \
    -i $INDEX \
    -o quant/$FASTQ \
    -1 $DATA$FASTQ"_R1.fastq.gz" \
    -2 $DATA$FASTQ"_R2.fastq.gz"
done
