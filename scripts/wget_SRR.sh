#!/bin/bash

############################################
# Récupération des fichiers fastq depuis SRA
############################################

# Dossier de travail
WKD=~/transcriptomic_analysis_ed-vaam/data/
cd $WKD

############################################

# Transcriptome de référence

# README
# ```
# Arabidopsis thaliana Genome Annotation Official Release (Approved by NCBI GenBank)
# Version: Araport11
# Original Araport11 Release date: June 2016
# Date last updated: Oct 1 2023 
# ```

# Readme : Araport11_cdna_20220914.gz  	- Transcript sequences in FASTA format
wget https://www.arabidopsis.org/download_files/Genes/Araport11_genome_release/Araport11_blastsets/Araport11_cdna_20220914_representative_gene_model.gz
gzip -d Araport11_cdna_20220914_representative_gene_model.gz
wget https://www.arabidopsis.org/download_files/Genes/Araport11_genome_release/Araport11-Subcellular_Predictions

############################################

# Analyse transcriptimique
SAMPLES=(
  "SRR15992263"
  "SRR15992269"
  "SRR15992277"
  "SRR15992265"
  "SRR15992272"
  "SRR15992281"
  "SRR15992267"
  "SRR15992274"
  "SRR15992283"
)

for FASTQ in ${SAMPLES[@]}; do
  wget https://sra-pub-run-odp.s3.amazonaws.com/sra/$FASTQ/$FASTQ
  fastq-dump --split-3 --gzip $FASTQ
done

mv SRR15992263_1.fastq.gz R1-WS-ZME-Ctrl_R1.fastq.gz
mv SRR15992263_2.fastq.gz R1-WS-ZME-Ctrl_R2.fastq.gz
mv SRR15992269_1.fastq.gz R2-WS-ZME-Ctrl_R1.fastq.gz
mv SRR15992269_2.fastq.gz R2-WS-ZME-Ctrl_R2.fastq.gz
mv SRR15992277_1.fastq.gz R3-WS-ZME-Ctrl_R1.fastq.gz
mv SRR15992277_2.fastq.gz R3-WS-ZME-Ctrl_R2.fastq.gz

mv SRR15992265_1.fastq.gz R1-PW-ZME-Ctrl_R1.fastq.gz
mv SRR15992265_2.fastq.gz R1-PW-ZME-Ctrl_R2.fastq.gz
mv SRR15992272_1.fastq.gz R2-PW-ZME-Ctrl_R1.fastq.gz
mv SRR15992272_2.fastq.gz R2-PW-ZME-Ctrl_R2.fastq.gz
mv SRR15992281_1.fastq.gz R3-PW-ZME-Ctrl_R1.fastq.gz
mv SRR15992281_2.fastq.gz R3-PW-ZME-Ctrl_R2.fastq.gz

mv SRR15992267_1.fastq.gz R1-PW-ZME-Sc_R1.fastq.gz
mv SRR15992267_2.fastq.gz R1-PW-ZME-Sc_R2.fastq.gz
mv SRR15992274_1.fastq.gz R2-PW-ZME-Sc_R1.fastq.gz
mv SRR15992274_2.fastq.gz R2-PW-ZME-Sc_R2.fastq.gz
mv SRR15992283_1.fastq.gz R3-PW-ZME-Sc_R1.fastq.gz
mv SRR15992283_2.fastq.gz R3-PW-ZME-Sc_R2.fastq.gz
