#!/usr/bin/python3

from Bio import SeqIO

data = "../../data/Araport11_cdna_20220914_representative_gene_model"
record_dict = SeqIO.to_dict(SeqIO.parse(data, "fasta"))

outfile = open("./Araport11_cdna_20220914_annotation.txt", 'w', encoding = 'utf-8')
headers = ["id", "Symbols", "Annotations"]
outfile.write("\t".join(headers) + "\n")

# Récupération des informations pour chaque mRNA
for cdna in list(record_dict.keys()):
  desc = record_dict[cdna].description.split("|")
  symbols = desc[1].split(":")[1]
  out = [cdna, symbols, desc[2]]
  outfile.write("\t".join(out) + "\n")

outfile.close()