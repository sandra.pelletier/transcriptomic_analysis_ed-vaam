# ED VAAM : Analyses transcriptomiques

![CC BY-NC 4.0 DEED](http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-nc.png){width=10%}

## Organisation des dossiers et principes FAIR.

Les quatres principes [FAIR](https://fr.wikipedia.org/wiki/Fair_data) sont : 

- Facile à trouver (*findable*) 
- Accessible (*accessible*)
- Interopérable (*interoperable*)
- Réutilisable (*reusable*)

Ils permettent de travailler dans le cadre de la science ouverte et le partage des données. Le respect des principes FAIR dès le début d'un projet assure une bonne organisation dans la construction et le stockage des données et facilite la présentation et la publication des résultats.

Les données seront ainsi organisées dés le début en trois grands groupes, et donc trois dossiers :

- Data
- Results   
- Scripts

### Data

Ce dossier regroupe l'ensemble des données brutes et données de référence. 

Dans le cadre de cette analyse, nous y trouverons :

- les fichiers fq.gz des données de séquençage
- le fichier fasta du transcriptome de référence
- le fichier d'annotation fonctionnelle
- tout autre fichier de données de référence (ex: gff)

Dans le cadre d'une analyse FAIR et dans l'objectif d'une publication, l'ensemble de ces données sera accessible, et donc référencé dans des dépôts publics (data.gouv, ncbi, ...).

> L'étude prise ici en exemple est [Guillou *et al.*, 2022](https://doi.org/10.1093/jxb/erac240). Les données sont accessibles sur le portail GEO du NCBI : [GSE184508](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE184508)
> 
> |SRR accession|GSM accession|   Sample name             |
> |---          |---          |---                        |
> | SRR15992263 | GSM5590465  | R1-Ws_ZME                 |
> | SRR15992269 | GSM5590471  | R3-Ws_ZME                 |
> | SRR15992277 | GSM5590479  | R4-Ws_ZME                 |
> | SRR15992265 | GSM5590467  | R1-proscoop12_ZME         |
> | SRR15992272 | GSM5590474  | R3-proscoop12_ZME         |
> | SRR15992281 | GSM5590483  | R4-proscoop12_ZME         |
> | SRR15992267 | GSM5590469  | R1-proscoop12+SCOOP12_ZME |
> | SRR15992274 | GSM5590476  | R3-proscoop12+SCOOP12_ZME |
> | SRR15992283 | GSM5590485  | R4-proscoop12+SCOOP12_ZME |

L'analyse faite sera les comparaisons suivantes : 
- PW-Ctrl versus WS-Ctrl : comparaison du mutant et du sauvage
- PW-Sc versus PW-Ctrl : comparaison du mutant + son peptide avec le mutant
- PW-Sc versus WS-Ctrl : comparaison du mutant + son peptide avec le sauvage

![alt text](Comparaisons.png){width=50%}

### Results

Les résultats pourront être organisés en sous dossiers par analyse. Les données sont les produits des analyses. Dans le cadre d'une analyse FAIR, ces données seront reproductibles.

Dans l'objectif d'une publication, ce sont les résultats qui seront présentés et discutés dans la publication.

### Scripts

Les scripts seront versionnés via un dépot [git](https://fr.wikipedia.org/wiki/Git) sur une forge (ex: gitHub ou gitLab). Ils seront ainsi facile à gérer et partager (principe FAIR).

Pour assurer la reproductiblilité des analyses, un environnement virtuel (ex: [conda](https://docs.conda.io/projects/conda/en/stable/)) permettra de fixer et traçer les versions des logiciels utilisés.

Dans l'objectif d'une publication, le dépot git sera ouvert au public et référencé sur [Software Heritage](https://docs.conda.io/projects/conda/en/stable/) qui attribura un identifiant unique.

#### Création de l'environnement CONDA

```{}
conda create --name transcriptomic_analysis_ed-vaam
conda activate transcriptomic_analysis_ed-vaam
conda install bioconda::sra-tools
conda install anaconda::wget
conda install bioconda::salmon
conda install conda-forge::r-base
conda install bioconda::bioconductor-deseq2
conda env export > environment.yml
```

## TP : Analyse de données transcriptomique

### Récupération des scripts

> Prérequis : avoir git sur sa machine

```{}
git clone https://forgemia.inra.fr/sandra.pelletier/transcriptomic_analysis_ed-vaam.git
cd transcriptomic_analysis_ed-vaam
```

### Environnement conda

> Prérequis : avoir conda sur sa machine

```{}
conda env create -f environment.yml
conda activate transcriptomic_analysis_ed-vaam
```

### Téléchargement des données

Téléchargement : utilisation du script wget_SRR.sh

```{}
mkdir data
cd data
bash ../scripts/wget_SRR.sh
```

L'une des suites logiques d'une analyse transcriptomique est :

- Alignement des séquences (*mapping*)
- Analyse différentielle
  - analyse statistique
  - étude par profil transcriptomique
  - bibliographie 
  
### Alignement des séquences

> Documentation [salmon](https://salmon.readthedocs.io/en/latest/salmon.html)

#### Index

La première étape consiste à créer un index à partir du transcriptome de référence. 

> Edition du fichier scripts/mapping_index.sh

- Se positionner dans le dossier de résultats
- Créer un nouveau dossier `mapping`
- Se déplacer dans ce dossier mapping
- Exécuter le script qui va créer l'index

```{}
mkdir results
cd results
mkdir mapping
cd mapping/
bash ../../scripts/mapping_index.sh
```

Un dossier index a été créé contenant l'index et des informations sur la construction de cet index. Un fichier `duplicate_clusters.tsv` donne la liste des séquences fasta dupliquées.

#### Quantification

Une fois l'index créé, l'étape suivante est l'alignement et le comptage des reads.

> Edition du fichier scripts/mapping_quant.sh

- Rester dans le dossier ./results/mapping/
- Executer le script permettant le comptage des reads

```{}
bash ../../scripts/mapping_quant.sh
```

### Analyse différentielle

Après l'alignement et le comptage des reads, l'analyse différentielle ce fait avec DESeq2 sous R. Au préalable, on construira un fichier `comparisons.txt` tabulé décrivant les différentes comparaisons envisagées contenant :
- Comparison : le nom de la comparaison
- File : le chemin relatif ou absolu des fichiers
- Name : le nom regroupant les différentes répétitions de l'échantillon
- Group : le type d'échantillon : `Ttmt` ou `Control`

> exemple de fichier comparisons.txt
> 
> | Comparison        | File                        | Name    | Group   |
> |---                |---                          |---      |---      |
> | mutant_vs_sauvage | ./path/file_mutant_rep1.sf  | mutant  | Ttmt    |
> | mutant_vs_sauvage | ./path/file_mutant_rep2.sf  | mutant  | Ttmt    |
> | mutant_vs_sauvage | ./path/file_sauvage_rep1.sf | sauvage | Control |
> | mutant_vs_sauvage | ./path/file_sauvage_rep1.sf | sauvage | Control |
> 

Un fichier `comparisons.txt` correspondant aux fichiers de séquençage téléchargés est fourni.

```{}
cd ..
mkdir deseq2
mv ../comparisons.txt ./deseq2/
cd deseq2
Rscript ../../scripts/deseq2.R comparisons.txt > r.log
```

#### AnaDiff

L'outil [AnaDiff](https://zenodo.org/records/6477918) permet de faire ces analyses avec DESeq2 et edgeR. Il permet également de générer un fichier résumant les analyses ainsi que des fichiers html qui, ouverts sous Excel, donne une mise en forme conditionnelle en couleur.
Le script construit est fourni : `scripts/AnaDiff_20210702.R`. 

### Compilation des résultats et des annotations des gènes

```{}
cd ..
mkdir analyse
cd analyse
python3 ../../scripts/fasta_annotation.py
Rscript ../../scripts/analyse.R > r.log
```

> Dans certains cas, les annotations provenant du fichier fasta ne sont pas correctement encodées. Ce problème peut soit empêcher l'ouverture du fichier sous excel ou libre office calc, soit provoquer des erreurs dans la compilation des fichiers et fusionner des cellules (vérifier le nombre de ligne du fichier excel).

### Analyse des résultats

La liste des gènes différentiellement exprimés représente le phénotypage moléculaire. Pour le comprendre et l'interpréter, il faut le regarder vis-à-vis du phénotypage physiologique. Cependant, il faut garder à l'esprit qu'il y a souvent un décalage temporel entre ces deux phénotype moléculaire et physiologique. Ce décalage de temps est différent en fonction des organes, des voies métaboliques en place, ...

Les messages importants à retenir lorsqu'on regarde les chiffres sont : 
- Les données d'un gènes ne sont exploitable que s'il y a une pvalue significative.
- Il ne faut pas regarder ce qui est au niveau du bruit de fond.
- La répétabilité d'une expérience impact la statistique.
